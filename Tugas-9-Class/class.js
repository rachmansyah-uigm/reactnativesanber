// 1. Animal Class
// Release 0
class Animal {
    constructor(name) {
        this._name = name
        this._legs = 4
        this._cold_blooded = false
    }
    get name() {
        return this._name;
      }
      get legs() {
        return this._legs;
      }
      get cold_blooded() {
        return this._cold_blooded;
      }
}

var sheep = new Animal("shaun");
console.log("Release 0") 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Release 1
console.log("\nRelease 1")
class Ape extends Animal {
    constructor(name) {
        super(name);
        this._legs = 2
      }
      yell() {
        console.log("Auooo") 
      }
}
class Frog extends Animal {
    constructor(name) {
        super(name);
        this._cold_blooded = true
      }
      jump() {
        console.log("hop hop") 
      }
}
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

// 2. Function to Class
class Clock {
    constructor({ template }) {
      this.template = template;
    }
  
    render() {
      let date = new Date();
  
      let hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      let mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      let secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop() {
      clearInterval(this.timer);
    }
  
    start() {
      this.render();
      this.timer = setInterval(() => this.render(), 1000);
    }
  }

var clock = new Clock({template: 'h:m:s'});
clock.start();