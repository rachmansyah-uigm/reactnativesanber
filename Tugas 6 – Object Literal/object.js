// Soal No. 1 (Array to Object)
console.log("Soal No. 1 (Array to Object)")
var now = new Date()
var thisYear = now.getFullYear()
function arrayToObject(peopleArr) {
    var peoples = [] 
    for(i = 0; i < peopleArr.length; i++) {
        if(thisYear < peopleArr[i][3] || peopleArr[i][3]==null) {
            ages = "Invalid birth year"
        } else {
            ages = thisYear - peopleArr[i][3]
        }
        peoples.push({
            firstName:peopleArr[i][0],
            lastName:peopleArr[i][1],
            gender: peopleArr[i][2],
            age: ages
        }) 
    }
    return peoples
}
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
console.log(arrayToObject(people))
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
console.log(arrayToObject(people2))

// Soal No. 2 (Shopping Time)
console.log("\nSoal No. 2 (Shopping Time)")
function shoppingTime(memberId, money) {
    var startMoney = money
    var listPrice = [["Sepatu Stacattu",1500000],["Baju Zoro",500000],["Baju H&N",250000],["Sweater Uniklooh",175000],["Casing Handphone",50000]]
    var i=0
    list=[]
    if(memberId == '') {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    if(money < 50000){
        return "Mohon maaf, uang tidak cukup"
    }
    if (arguments.length == 0) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    while(money >= 50000 && i!=5) {
        if (money >= listPrice[i][1]) {
            list.push(listPrice[i][0])
            money-=listPrice[i][1]
        }
        i++
    }
    var shopping = {
        memberId: memberId,
        money: startMoney,
        listPurchased:list,
            changeMoney: money
    }
    return shopping
  }
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000)); 
console.log(shoppingTime()); 
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 100000));

// Soal No. 3 (Naik Angkot)
console.log("\nSoal No. 3 (Naik Angkot)")
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    routeArr =[]
    for (var i=0; i < arrPenumpang.length; i++) {
        cost = (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1])) * 2000
        var route = {
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: cost
        }
        routeArr.push(route)
    }
    return routeArr
  }
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));