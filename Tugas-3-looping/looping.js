// No. 1 Looping While 
var count = 2;
console.log("LOOPING PERTAMA");
while(count <= 20) {
    console.log(count + " - I love coding");
    count+=2;
}

count = 20;
console.log("\nLOOPING KEDUA");
while(count >= 2) {
    console.log(count + " - I love coding");
    count-=2;
}
console.log('\n');

// No. 2 Looping menggunakan for
for(var i = 1; i <= 20; i++) {
    if ((i % 3 == 0 ) && (i % 2 == 1)) {
        console.log(i + " - I Love Coding");
    } else if (i % 2 == 0) {
        console.log(i + " - Berkualitas");
    } else {
        console.log(i + " - Santai");
    }
 } 
 console.log("\n");

// No. 3 Membuat Persegi Panjang #
for (var i = 1; i <= 4; i++) {
     for (var j = 1; j <= 8; j++) {
        process.stdout.write('#');
     }
     process.stdout.write('\n');
 }
 process.stdout.write('\n');

// No. 4 Membuat Tangga 
for (var i = 1; i <= 7; i++) {
    for (var j = 1; j <= i; j++) {
       process.stdout.write('#');
    }
    process.stdout.write('\n');
}
process.stdout.write('\n');

// No. 5 Membuat Papan Catur
for (var i = 1; i <= 8; i++) {
    for (var j = 1; j <= 4; j++) {
        if (i % 2 == 0) {
            process.stdout.write('# ');
        } else {
            process.stdout.write(' #');
        }
       
    }
    process.stdout.write('\n');
}
process.stdout.write('\n');

