
var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var bookRemaining = books.length
var time = 10000

function reading(time, idx, bookRemaining) {
    readBooksPromise(time, books[idx])
    .then(function (timeRemaining) {
        time = timeRemaining
        console.log('Sisa waktu', time)
        bookRemaining -= 1
        if(bookRemaining > 0) {
            reading(time, idx+1, bookRemaining)
        }
    })
    .catch(function(error) {
    })
}
reading(time, 0, bookRemaining)

