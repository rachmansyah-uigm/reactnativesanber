// Soal No. 1 (Range)
console.log("Soal No. 1 (Range)")
function range(startNum = -1, finishNum = -1) {
    var numbers = [];
    var i = 0;
    if (arguments.length < 2) {
        return -1
    }
    if (startNum < finishNum) {
        numbers.splice(0, numbers.length)
        for (i = startNum; i <= finishNum; i++) {
            numbers.push(i);
        }
    } 
    if(startNum > finishNum) {
        numbers.splice(0, numbers.length)
        for(i = startNum; i >= finishNum; i--) {
            numbers.push(i);
        }
    }
    return numbers
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Soal No. 2 (Range with Step)
console.log("\nSoal No. 2 (Range with Step)")
function rangeWithStep(startNum, finishNum, step) {
    var numbers = [];
    var i = 0;
    if (arguments.length < 2) {
        return -1
    }
    if (startNum < finishNum) {
        numbers.splice(0, numbers.length)
        for (i = startNum; i <= finishNum; i+=step) {
            numbers.push(i);
        }
    } 
    if(startNum > finishNum) {
        numbers.splice(0, numbers.length)
        for(i = startNum; i >= finishNum; i-=step) {
            numbers.push(i);
        }
    }
    return numbers
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal No. 3 (Sum of Range)
console.log("\nSoal No. 3 (Sum of Range)")
function sum(beginNum,endNum,step=1) {
    if (arguments.length == 1) {
        return 1
    }
    var sum=0;
    var arrSum = rangeWithStep(beginNum,endNum,step)
    for (var i = 0; i < arrSum.length; i++) {
        sum += arrSum[i]
    }

    return sum
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal No. 4 (Array Multidimensi)
console.log("\nSoal No. 4 (Array Multidimensi)")
function dataHandling (data) {
    for (var i = 0; i < data.length; i++) {
        console.log("Nomor ID:  " + data[i][0] + "\n" +
                    "Nama Lengkap: " + data[i][1] + "\n" +
                    "TTL:  " + data[i][2] + " " + data[i][3] + "\n" +
                    "Hobi: " + data[i][4]
        )
     }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
dataHandling(input)
